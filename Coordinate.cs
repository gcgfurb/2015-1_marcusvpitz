﻿using UnityEngine;
using System.Collections;

/**
* Classe responsavel por modelar as Coordenadas de Latitude e Longitude (geograficas)
**/
public class Coordinate {

	private float latitude;
	private float longitude;
	
	private float latitudeRad;
	private float longitudeRad;
	
	public Coordinate(float latitude, float longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.latitudeRad = latitude * (Mathf.PI / 180.0f);
		this.longitudeRad = longitude * (Mathf.PI / 180.0f);
	}
	
	public float getLatitude() {
		return this.latitude;
	}
	
	public float getLatitudeRad() {
		return this.latitudeRad;
	}
	
	public float getLongitude() {
		return this.longitude;
	}
	
	public float getLongitudeRad() {
		return this.longitudeRad;
	}

}
