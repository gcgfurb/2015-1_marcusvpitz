TCC's Project by Marcus Vin�cius Pitz
Oriented by Dalton Solano dos Reis

TCC2 2015-01, Regional University of Blumenau - FURB.


ABSTRACT

This paper presents the construction of an application for the Android platform, which features a virtual three-dimensional map, which through geolocation and geo-referencing capabilities, allows display on the map the user's position represented by an avatar and sites with relevant information. It consists of a application for the Android platform, three-dimensional map of the area of the blocks R, S and T of Foundation Regional University of Blumenau (FURB) and an avatar that represents the user in virtual map. We used the Unity 3D framework for application creation, Blender software for modeling the map and Avatar, the GPS sensor to obtain the user's geographical coordinates and compass sensor to obtain the user guidance. During the development are presents concepts and techniques of geolocation and georeferencing, such as using Kalman filter to correct coordinate values and the Haversine formula for distance between coordinates. As a result it developed an application that allows the user to locate the three-dimensional map through their avatar and view points of interest available on the map, both based on their position.


Key-words: Three-dimensional map. Georeferencing. Android map. Unity 3D. Geolocation. Kalman filter. Haversine formula.
