﻿using UnityEngine;
using System.Collections;

/**
* Classe responsavel pelos calculos de GEO REFERENCIAMENTO
**/
public class GEO {

	public const float WORLD_RADIUS_KILOMETERS = 6371.0f;
	public const float WORLD_RADIUS_METERS = WORLD_RADIUS_KILOMETERS * 1000.0f;
	
	/**
	* Retorna a distancia em metros entre duas coordenadas Geograficas
	**/
	public static float refDistanceMeters(Coordinate from, Coordinate to) {
		float step1 = Mathf.Sqrt( 1.0f - ( Mathf.Pow(Mathf.Sin( (to.getLatitudeRad() - from.getLatitudeRad())/2.0f ),2.0f) 
		                                  + Mathf.Cos(from.getLatitudeRad()) * Mathf.Cos(to.getLatitudeRad()) 
		                                  * Mathf.Pow(Mathf.Sin( (to.getLongitudeRad() - from.getLongitudeRad())/2.0f ),2.0f) ) );
		
		float step2 = Mathf.Sqrt( ( Mathf.Pow(Mathf.Sin( (to.getLatitudeRad() - from.getLatitudeRad())/2.0f ),2.0f) 
		                          + Mathf.Cos(from.getLatitudeRad()) * Mathf.Cos(to.getLatitudeRad()) 
		                          * Mathf.Pow(Mathf.Sin( (to.getLongitudeRad() - from.getLongitudeRad())/2.0f ),2.0f) ) );
		                        
		float step3 = Mathf.Atan(step2/step1);
		
		return WORLD_RADIUS_METERS * 2.0f * step3;
	}
	
}
