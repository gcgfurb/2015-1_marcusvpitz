--PEGAR DISTANCIA ENTRE DOIS PONTOS DE LATITUDE E LONGITUDE

LEMBRANDO QUE � NESSARIO CONVERTER OS GRAUS DA LATITUDE E LONGITUDE PARA RADIANOS (SE A BIBLIOTECA TRABALHAR COM RAD...)


-----------------------


    public double getDistancia(double latitude, double longitude, double latitudePto, double longitudePto){  
				double dlon, dlat, a, distancia;  
				dlon = longitudePto - longitude;  
				dlat = latitudePto - latitude;  
				a = Math.pow(Math.sin(dlat/2),2) + Math.cos(latitude) * Math.cos(latitudePto) * Math.pow(Math.sin(dlon/2),2);  
				distancia = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));  
				return 6378140 * distancia; /* 6378140 is the radius of the Earth in meters*/  
        }  