﻿using UnityEngine;
using System.Collections;

public class TerrainHandler : MonoBehaviour {
	public float angleNorthRel = -21.3f;
	
	public float latFrom = 0.0f;
	public float longFrom = 0.0f;
	
	public float latTo = 0.0f;
	public float longTo = 0.0f;
	
	void Awake () {
		if (angleNorthRel == -1.0f) {
			angleNorthRel = MATHF_GEO.getAngle( new Coordinate(latFrom, longFrom), new Coordinate(latTo, longTo) );
		}
		this.transform.Rotate(0.0f, angleNorthRel, 0.0f);
	}
}
