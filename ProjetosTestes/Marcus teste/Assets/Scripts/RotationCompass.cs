﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RotationCompass : MonoBehaviour {	
	void Awake() {
		CONFIG_VALUES.canCompass = true;
	}

	void Start () {
		Input.compass.enabled = true;	
	}
	
	void Update () {	
		if (CONFIG_VALUES.canCompass) {
			float compassRot = MATHF_GEO.normalizeEulerDegress( Input.compass.magneticHeading );
			Quaternion to = Quaternion.Euler(0.0f
			                               , compassRot 
			                               , 0.0f);
			this.transform.rotation = Quaternion.Slerp(this.transform.rotation, to, Time.deltaTime);		
		}
	}		
}
