﻿using UnityEngine;
using System.Collections;

/**
* Classe responsavel por modelar as Coordenadas de Latitude e Longitude (geograficas)
**/
public class Coordinate {

	public float latitude;
	public float longitude;	
	public float latitudeRad;
	public float longitudeRad;	
	
	public Coordinate() {}
				
	public Coordinate(float latitude, float longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.latitudeRad = latitude * (Mathf.PI / 180.0f);
		this.longitudeRad = longitude * (Mathf.PI / 180.0f);
	}
	
	public float getLatitude() {
		return this.latitude;
	}
	
	public float getLatitudeRad() {
		return this.latitudeRad;
	}
	
	public float getLongitude() {
		return this.longitude;
	}
	
	public float getLongitudeRad() {
		return this.longitudeRad;
	}
	
	public Coordinate clone() {
		return new Coordinate(this.latitude, this.longitude);
	}
	
	public string ToString() {
		return "Coord lat " + this.latitude + ", long " + this.longitude + ". Coord rad " + this.latitudeRad + ", " + this.longitudeRad;
	}
	
	public static bool operator ==(Coordinate c1, Coordinate c2) {
		return ((c1.getLatitude() == c2.getLatitude()) && (c1.getLongitude() == c2.getLongitude()));
	}
	
	public static bool operator !=(Coordinate c1, Coordinate c2) {
		return ((c1.getLatitude() != c2.getLatitude()) || (c1.getLongitude() != c2.getLongitude()));
	}
	
	public static Coordinate operator +(Coordinate c1, Coordinate c2) {
		return new Coordinate(c1.getLatitude() + c2.getLatitude(), c1.getLongitude() + c2.getLongitude());
	}
	
	public static Coordinate operator -(Coordinate c1, Coordinate c2) {
		return new Coordinate(c1.getLatitude() - c2.getLatitude(), c1.getLongitude() - c2.getLongitude());
	}
	
	public static Coordinate operator *(float coeficient, Coordinate c1) {
		return new Coordinate(c1.getLatitude() * coeficient, c1.getLongitude() * coeficient);
	}
	
	public static Coordinate operator *(Coordinate c1, float coeficient) {
		return new Coordinate(c1.getLatitude() * coeficient, c1.getLongitude() * coeficient);
	}

}
