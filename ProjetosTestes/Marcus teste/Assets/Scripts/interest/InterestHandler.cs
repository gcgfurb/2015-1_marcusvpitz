﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InterestHandler : MonoBehaviour {

	public GameObject anchor;

	public float clock = 1.0f;
	
	private float actualTime = 0.0f;

	private ArrayList points = new ArrayList();

	void Awake() {
		PrefUtils.createDefaultsInterest();
		PrefUtils.MAIN_CAMERA = this.transform;
	}

	void Start () {
		ArrayList inter = PrefUtils.getAllInterestPoint();		
		for (int i = 0; i < inter.Count ; i++) {
			points.Add( new InterestPointREF( (InterestPoint) inter[i] ) );
		}	
	}
	
	// Update is called once per frame
	void Update () {
		if (actualTime >= clock) {
			valid();
			actualTime = 0.0f;	
		} else {
			actualTime = actualTime + Time.deltaTime;
		}
	}
	
	private void valid() {
		for (int i = 0; i < points.Count ; i++) {
			InterestPointREF refP = (InterestPointREF) points[i];
			if (refP.valid()) {				
				GameObject gb = (GameObject)Instantiate(anchor, refP.getPos(), Quaternion.Euler(0.0f, 0.0f, 0.0f));
				
				/**
				* TODO assume the first element
				**/
				(gb.GetComponentsInChildren<TextMesh>())[0].text = refP.getPoint().description;	
				
				refP.setGameObject(gb);
			}
		}
	}
}
