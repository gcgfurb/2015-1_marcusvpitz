﻿using UnityEngine;
using System.Collections;

public class InterestPoint {
	public string title;
	public string description;
	public Coordinate coord;
	public float distance;
	
	public InterestPoint() {
	}
	
	public InterestPoint(string title, string description, Coordinate coord, float distance) {		
		this.title = title;
		this.description = description;
		this.coord = coord;
		this.distance = distance;
	}		
	
	public string ToString() {
		return "Title=" + title + ", description=" + description + ", distance=" + distance + ", " + coord.ToString();
	}
}

public class InterestPointREF {
	private InterestPoint point;
	private Vector3 pos;
	private GameObject point3D = null;
	
	public InterestPointREF(InterestPoint point) {
		this.point = point;		
		this.pos = GEO.refGeoLogicalPosition(this.point.coord);
	}
	
	public bool valid() {		
		if (MATHF_GEO.Dc(pos) > point.distance) {
			//clear
			if (point3D != null) {
				MonoBehaviour.Destroy(point3D);
				point3D = null;
			}
			return false;
		} else {
			if (point3D == null) {
				//Instantiate
				return true;
			} 
			return false;
		}
	}
	
	public Vector3 getPos() {
		return this.pos;
	}
	
	public void setGameObject(GameObject gb) {
		this.point3D = gb;
	}
	
	public InterestPoint getPoint() {
		return this.point;
	}
}