﻿using UnityEngine;
using System;
using System.Xml.Serialization;
using System.IO;
using System.Collections;

public static class ExtensibleMarkupSerial {
	
	public static object deSerialize(string content, Type t) {				
		object result = null;
		try {
			XmlSerializer xml = getXml(t);		
			result = deSerialize(content, xml);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}
	
	private static object deSerialize(string content, XmlSerializer xml) {				
		object result = null;
		StringReader str = new StringReader(content);
		result = xml.Deserialize(str);
		return result;
	}
	
	public static string serialize(object content, Type t) {				
		string result = "";
		try {
			XmlSerializer xml = getXml(t);		
			StringWriter str = new StringWriter();
			xml.Serialize(str, content);
			result = str.ToString();
		} catch (Exception e) {
			result = "";
		}
		return result;
	}
	
	private static XmlSerializer getXml(Type t) {
		Type[] tp = TypeRelation.getRelationTypes(t);
		if (tp != null) {
			return new XmlSerializer(t, tp);
		} else {
			return new XmlSerializer(t);
		}
	}
	
}
