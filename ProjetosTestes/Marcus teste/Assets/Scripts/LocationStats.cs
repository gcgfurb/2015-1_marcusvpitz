﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class LocationStats : MonoBehaviour {
	
	public Text texto;
	public float rotationVelocity = 3.0f;	
	private bool firstApplyMove = false;
	private bool isGpsMove = false;
	
	private Coordinate coord;
	
	private InputLocation inLoc;
	
	private Vector3 toPosV;
	private Vector3 fromV;
	private Vector2 centerJoy;
	
	private float distance = 0.0f;
	private float amountWalk = 0.0f;
	private float amountWalkInterpol = 0.0f;
	
	private int fingerId = -1;
	
	//FPS METER
	private float timeSecond = 0.0f;
	private int countFrames = 0;
	private int lastFrames = 0;
	
	/**
	* TODO Get person
	**/
	public static Transform mainStudent;
		
	void Awake() {
		mainStudent = this.transform;
	}
		
	public void Start () {					
		firstApplyMove = true;
		this.transform.position = new Vector3(CenterMap.centerCoord.position.x, this.transform.position.y, CenterMap.centerCoord.position.z);		
		
		if (Input.location.isEnabledByUser) {			
			texto.text = "GPS ATIVADO";
			inLoc = new InputGPS();	
			isGpsMove = true;	
		} else {
			texto.text = "GPS DESATIVADO";
			inLoc = new DisableInputByGpsUnknow();		
			CONFIG_VALUES.canCompass = false;	
			isGpsMove = false;
		}		
		centerJoy = new Vector2(Screen.width/2.0f, Screen.height/2.0f);		
		//inLoc = new DebuggableInput();
		//inLoc = new DebuggableInputBlocoRST();
		
		this.coord = CenterMap.mapCenter.clone();	
		toPosV = Vector3.zero;	
		texto.color = Color.red;
		this.gameObject.animation["walk"].speed = CONFIG_VALUES.AVERAGE_VELOCITY_PERSON_IN_METERS;		
	}
	
	private void verifyInput() {
		if (Input.location.isEnabledByUser) {
			if (!isGpsMove) {
				texto.text = "GPS ATIVADO";
				inLoc = new InputGPS();
				CONFIG_VALUES.canCompass = true;
				isGpsMove = true;	
			}
		} else {
			if (isGpsMove) {
				texto.text = "GPS DESATIVADO";
				inLoc = new DisableInputByGpsUnknow();		
				CONFIG_VALUES.canCompass = false;	
				isGpsMove = false;
			}
		}
	}		
	
	// Update is called once per frame
	public void Update () {	
		verifyInput();
		if (inLoc.isPrepared()) {		
			
			//Captura a coordenada do INPUT padrao
			Coordinate newC = inLoc.get();													
			
			//Change geo position
			if (newC != this.coord) {				 
				Coordinate toCoord = newC;
				//PONTO PIVO
				toPosV = GEO.refGeoLogicalPosition(toCoord);
				this.coord = toCoord;							
				this.changeValuesByPos(toPosV);
			}
			if (toPosV != Vector3.zero) {
				//this.transform.position = toPosV;				
				if (firstApplyMove) {
					this.transform.position = toPosV;					
					firstApplyMove = false;				
				} else {								
					this.walkInterpolRef();				
				}
			}
		} else {
			texto.text = inLoc.getError();
			texto.text = texto.text + "\nLiberando movimentaçao por joystick";
			joyMov();
		}		
		idle ();
		/**
		if (this.timeSecond >= 1.0f) {
			lastFrames = this.countFrames;			
			this.timeSecond = 0.0f;
			this.countFrames = 0;
		} else {
			this.countFrames++;
			this.timeSecond = this.timeSecond + Time.deltaTime;
		}
		texto.text = texto.text + "\nFPS=" + this.lastFrames; **/
	}		
	
	private void joyMov() {
		for(int i = 0; i < Input.touches.Length; i ++) {
			Touch touch=Input.touches[i];
			if(touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
			{
				if (fingerId == touch.fingerId) {
					float angle = MATHF_GEO.getAngle(this.centerJoy, touch.position);
					this.transform.rotation = Quaternion.Euler(this.transform.rotation.x, angle, this.transform.rotation.z);				
					this.transform.Translate(0f , 0f , Time.deltaTime * CONFIG_VALUES.AVERAGE_VELOCITY_PERSON_IN_METERS);				
					walk ();
				}
			} else if(touch.phase == TouchPhase.Began)
			{
				if (fingerId == -1) {
					fingerId = touch.fingerId;
				}
			} else if(touch.phase == TouchPhase.Ended)
			{
				if (fingerId == touch.fingerId) {
					fingerId = -1;
					stp();
				}
			}
		}		
	}
	
	private void walkInterpolRef() {
		if (this.amountWalk < 1.0f) {			
			this.amountWalk = this.amountWalk + (this.amountWalkInterpol * Time.deltaTime);			
			walk ();
		} else {			
			stp();
		}		
		/**
		* TODO Realizado um ajuste BELO e FINO para arrumar o problema do slerp ao variar o Y, mesmo o TO e FROM tendo Y iguais
		* sera salvo o Y e re-aplicado depois
		**/
		float yGambiBefore = this.transform.position.y;
		this.transform.position = Vector3.Slerp(this.fromV
		                                        , this.toPosV
		                                        , amountWalk);
		this.transform.position = new Vector3 (this.transform.position.x, yGambiBefore, this.transform.position.z);
		                                        
		this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(toPosV - fromV), rotationVelocity * Time.deltaTime);
	}
	
	private void changeValuesByPos(Vector3 toPosV) {		
		this.fromV = this.transform.position;
		this.amountWalk = 0.0f;
		this.distance = Vector3.Distance(new Vector3(this.fromV.x, 0.0f, this.fromV.z)
		                                ,new Vector3(toPosV.x, 0.0f, toPosV.z));			
		this.amountWalkInterpol = CONFIG_VALUES.AVERAGE_VELOCITY_PERSON_IN_METERS / distance;
	}	
	
	private void stp() {
		if(this.gameObject.animation.IsPlaying("walk")) {
			this.gameObject.animation.Stop("walk");
		}
	}	
	
	private void walk() {
		if(this.gameObject.animation.IsPlaying("idle")) {
			this.gameObject.animation.Stop("idle");		
		}
		this.gameObject.animation.Play("walk");
	}
	
	private void idle() {
		if(!this.gameObject.animation.IsPlaying("walk")) {
			this.gameObject.animation.Play("idle");
		}
	}		
}