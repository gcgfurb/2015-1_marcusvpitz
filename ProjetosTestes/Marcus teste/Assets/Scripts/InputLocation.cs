﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class InputLocation {

	private List<FilterNavigation> filters = new List<FilterNavigation>();
	protected Coordinate last = CenterMap.mapCenter;
	
	public InputLocation() {
		//Add a Kalman Filter with 1 meter accuracy
		filters.Add(new KalmanFilter(1.0f, CONFIG_VALUES.AVERAGE_VELOCITY_PERSON_IN_METERS));
	}
	
	public Coordinate get() {		
		Coordinate coord = generateCoord();
		//Change
		if (coord != last) {
			last = applyFilters(coord);
		}
		return last;
	}
	
	protected abstract Coordinate generateCoord();
	
	public Coordinate get_DEBUGABLE(float lat, float longi) {		
		Coordinate coord = new Coordinate(lat, longi);
		//Change
		if (coord != last) {
			return applyFilters(coord);
		}
		return last;
	}
	
	public abstract bool isPrepared();
	
	public abstract string getError();
	
	protected virtual Coordinate applyFilters(Coordinate coord) {
		Coordinate c = coord;
		foreach(FilterNavigation f in filters) {
			c = c * f;
		}
		return c;
	}
}

public class InputGPS : InputLocation {

	public InputGPS() {		
		//Accuracy, updateDistanceInMeters
		Input.location.Start(5.0f, 1.0f);
	}

	public override bool isPrepared() {
		return Input.location.status == LocationServiceStatus.Running;
	}
	
	public override string getError() {
		if (Input.location.status == LocationServiceStatus.Initializing) {
			return "\nO GPS ainda esta sendo iniciado, aguarde...";
		} else if (Input.location.status == LocationServiceStatus.Failed) {
			return "\nFalha no GPS, favor verificar conexao";
		}
		return "";
	}	
	
	protected override Coordinate generateCoord() {
		return new Coordinate(Input.location.lastData.latitude, Input.location.lastData.longitude);
	}
}

public class DebuggableInput : InputLocation {
		
	public override bool isPrepared() {
		return true;
	}
	
	public override string getError() {		
		return "";
	}	
	
	protected override Coordinate generateCoord() {
		if (Input.GetKeyDown(KeyCode.A)) {
			return new Coordinate(-26.894747f, -49.094943f);			
		} else if (Input.GetKeyDown(KeyCode.B)) {
			return new Coordinate(-26.894717f, -49.094859f);
		} else if (Input.GetKeyDown(KeyCode.C)) {
			return new Coordinate(-26.894887f, -49.094879f);
		} else if (Input.GetKeyDown(KeyCode.D)) {
			return new Coordinate(-26.894856f, -49.094794f);
		}
		return this.last;
	}
}

public class DebuggableInputBlocoRST : DebuggableInput {
	protected override Coordinate generateCoord() {
		if (Input.GetKeyDown(KeyCode.A)) {
			//return new Coordinate(-26.905569f, -49.079788f);
			//return new Coordinate(-26.905439f, -49.079955f);
			return new Coordinate(-26.905439f, -49.079955f);			
		} else if (Input.GetKeyDown(KeyCode.B)) {
			return new Coordinate(-26.905479f, -49.079899f);
		} else if (Input.GetKeyDown(KeyCode.C)) {
			return new Coordinate(-26.905785f, -49.079995f);
		} else if (Input.GetKeyDown(KeyCode.D)) {
			return new Coordinate(-26.905962f, -49.079362f);
		}
		return this.last;
	}
}

public class DisableInputByGpsUnknow : InputLocation {
	
	public override bool isPrepared() {
		return false;
	}
	
	public override string getError() {		
		return "GPS DESATIVADO";
	}	
	
	protected override Coordinate generateCoord() {
		return null;
	}
}
