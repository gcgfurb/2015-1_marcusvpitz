﻿using UnityEngine;
using System.Collections;

/**
* Classe responsavel pelos calculos de GEO REFERENCIAMENTO
**/
public class GEO {
	public const float WP_NORTH_POLE_DISTANCE_TRANSLATE = 99999.9f;
	public const float WORLD_RADIUS_KILOMETERS = 6371.0f;
	public const float WORLD_RADIUS_METERS = WORLD_RADIUS_KILOMETERS * 1000.0f;
	
	/**
	* Retorna a distancia em metros entre duas coordenadas Geograficas
	* Implementaçao do algoritimo Haversine
	**/
	public static float refDistanceMeters(Coordinate from, Coordinate to) {
		float step1 = Mathf.Sqrt( 1.0f - ( Mathf.Pow(Mathf.Sin( (to.getLatitudeRad() - from.getLatitudeRad())/2.0f ),2.0f) 
		                                  + Mathf.Cos(from.getLatitudeRad()) * Mathf.Cos(to.getLatitudeRad()) 
		                                  * Mathf.Pow(Mathf.Sin( (to.getLongitudeRad() - from.getLongitudeRad())/2.0f ),2.0f) ) );
		
		float step2 = Mathf.Sqrt( ( Mathf.Pow(Mathf.Sin( (to.getLatitudeRad() - from.getLatitudeRad())/2.0f ),2.0f) 
		                          + Mathf.Cos(from.getLatitudeRad()) * Mathf.Cos(to.getLatitudeRad()) 
		                          * Mathf.Pow(Mathf.Sin( (to.getLongitudeRad() - from.getLongitudeRad())/2.0f ),2.0f) ) );
		                        
		float step3 = Mathf.Atan(step2/step1);
		
		return WORLD_RADIUS_METERS * 2.0f * step3;
	}
	
	/**
	* Create a new GEO/LOGICAL position, based on GPS changes pos
	**/
	public static Vector3 refGeoLogicalPosition(Coordinate toGeoCoord) {				
		return refGeoLogicalPosition(CenterMap.mapCenter, toGeoCoord, CenterMap.centerCoord.position);
	}
	public static Vector3 refGeoLogicalPosition(Coordinate actualGeoCoord, Coordinate toGeoCoord, Vector3 actualLogicalPos) {
		float angle = MATHF_GEO.getAngle(actualGeoCoord, toGeoCoord);
		float distanceAmount = GEO.refDistanceMeters(actualGeoCoord, toGeoCoord);		
		float sin = Mathf.Sin(angle * Mathf.Deg2Rad);
		float cos = Mathf.Cos(angle * Mathf.Deg2Rad);
		Vector3 forwardDir = new Vector3( sin, 0.0f, cos);
		Vector3 result = actualLogicalPos + (forwardDir * distanceAmount);
		return result;
	}
	
}

public class MATHF_GEO {
	public static float normalizeEulerDegress(float degress) {
		if (degress > 180) {
			return degress - 360.0f;
		}
		return degress;
	}
	
	public static float getAngle(Coordinate from, Coordinate to) {
		Vector3 fromV = new Vector3(from.getLongitude(), 0.0f, from.getLatitude());
		Vector3 toV = new Vector3(to.getLongitude(), 0.0f, to.getLatitude());
		return getAngle(fromV, toV);
	}
	
	public static float getAngle(Vector2 fromV, Vector2 toV) {
		Vector3 fV = new Vector3(fromV.x, 0.0f, fromV.y);
		Vector3 tV = new Vector3(toV.x, 0.0f, toV.y);
		return getAngle(fV, tV);
	}
	
	public static float getAngle(Vector3 fromV , Vector3 toV)
	{
		int q;
		float CA,CO,H;
		float angle;
		Vector3 to;
		to = toV - fromV;
		// se os vetores forem iguais retorna 0
		if(to.x == to.z)
			return 0;
		if(to.x > 0)
		{
			if(to.z > 0)
				q = 0;
			else
				q = 1;
		}
		else if(to.z > 0)
			q = 3;
		else
			q = 2;
		
		// Adiciona to.x para o cateto adjascente		
		CA = to.x;
		// Adiciona to.y para o cateto oposto
		CO = to.z;
		// descobre a hipotenusa h² = CA² + CO²
		H = Mathf.Sqrt(Mathf.Pow(CA,2)+Mathf.Pow(CO,2));
		// descobre o cosseno cos = CA / H
		angle = CA / H;
		// descobre o angulo em radianos
		angle = Mathf.Acos(angle);
		// converte radianos em graus degress = radius * Mathf.Rad2Deg;
		angle =  angle * Mathf.Rad2Deg;		
		
		if(q == 0)
		{
			angle = angle - 90;
			angle = angle * -1;
		}else if(q == 3)
		{
			angle = angle - 90;
			angle = angle * -1;
		}else if(q == 2)
		{
			angle = angle - 90;
			angle = angle * -1;
			angle = -180 - angle;
		}else
		{
			angle = angle - 90;
			angle = angle * -1;
			angle = 180 - angle;
		}
		return angle;
	}
	
	public static float Dc(Vector3 p) {
		float ret = Mathf.Sqrt( Mathf.Pow(p.x - LocationStats.mainStudent.position.x, 2.0f) + Mathf.Pow(p.z - LocationStats.mainStudent.position.z, 2.0f) );
		return ret;
	}
}
