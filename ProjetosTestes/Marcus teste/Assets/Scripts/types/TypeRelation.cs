﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TypeRelation {

	public static Dictionary<Type, Type[]> TYPES_REL = new Dictionary<Type, Type[]>();
	
	//ADD TYPE RELATIONS
	static TypeRelation(){
		//InterestPoint
		TypeRelation.TYPES_REL.Add(typeof(InterestPoint), new Type[] {typeof(Coordinate)});
		
		/**
		* TODO Gambi para funcionar colection de InterestPoint
		**/
		TypeRelation.TYPES_REL.Add(typeof(ArrayList), new Type[] {typeof(InterestPoint),typeof(Coordinate)});
	}
	
	public static Type[] getRelationTypes(Type t) {
		if (TYPES_REL.ContainsKey(t)) {
			return TYPES_REL[t];
		}
		return null;
	}

}
