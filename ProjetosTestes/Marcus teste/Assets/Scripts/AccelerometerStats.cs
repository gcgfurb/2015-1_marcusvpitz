﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AccelerometerStats : MonoBehaviour {
	
	public Text texto;
	public Transform cubo;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//texto.text = "X=" + Input.acceleration.x + ", Y=" + Input.acceleration.y + ", Z=" + Input.acceleration.z;
		//this.transform.Translate(Input.acceleration.x, 0.0f, Input.acceleration.z);
		Quaternion to = Quaternion.Euler(1.0f, this.cubo.eulerAngles.y, 1.0f);					
		Quaternion from = Quaternion.Euler(1.0f, this.transform.rotation.eulerAngles.y, 1.0f);
		float exists = Quaternion.Angle(from, to);
		Debug.Log("Angle=" + exists);
	}
}
