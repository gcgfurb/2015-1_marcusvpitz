﻿using UnityEngine;
using System.Collections;

public class MapConsumer : MonoBehaviour {

	string exampleUrl = "http://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,"+
		"New+York,NY&zoom=13&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794"+
			"&markers=color:green%7Clabel:G%7C40.711614,-74.012318"+
			"&markers=color:red%7Ccolor:red%7Clabel:C%7C40.718217,-73.998284"+
			"&sensor=false";
	//string key = "&key=YOUR_API_KEY"; //put your own API key here.
	private WWW www;
	void Start() {
		//WWW www = new WWW(exampleUrl+key);
		www = new WWW(exampleUrl);
	}
	
	void Update() {
		if (www.isDone) {
			renderer.material.mainTexture = www.texture;
			www = new WWW(exampleUrl);
			Debug.Log("Feito");
		}
	}
	
}
