﻿using UnityEngine;
using System.Collections;

public static class PrefUtils {

	private static readonly string INTEREST_KEY = "INTEREST_POINTS"; 
	public static Transform MAIN_CAMERA;
	
	public static ArrayList getAllInterestPoint() {
		ArrayList arr;
		string content = PlayerPrefs.GetString(INTEREST_KEY, "");
		if (content.Length > 0) {
			arr = (ArrayList)ExtensibleMarkupSerial.deSerialize(content, typeof(ArrayList));
		} else {
			arr = new ArrayList();
		}
		return arr;
	}
	
	/**
	* CREATE DEFAULTS INTEREST POINTS
	**/
	public static void createDefaultsInterest() {
		string content = PlayerPrefs.GetString(INTEREST_KEY, "");
		if (content.Length == 0) {
			ArrayList interest = new ArrayList();
			
			//CANTINA A 10 METROS
			interest.Add( new InterestPoint("Cantina", "Cantina do bloco T", new Coordinate(-26.905439f, -49.079955f), 10.0f) );
			
			//SALA S-410
			interest.Add( new InterestPoint("S-410", "Sala S-410, bloco S", new Coordinate(-26.905768f, -49.079674f), 15.0f) );
			
			//BLOCO R
			interest.Add( new InterestPoint("Bloco R", "Bloco R, campus 1", new Coordinate(-26.905870f, -49.079992f), 20.0f) );
			
			//Entrada bloco I
			interest.Add( new InterestPoint("Bloco I", "Entrada de bloco I", new Coordinate(-26.905669f, -49.078987f), 12.0f) );
			
			//SALVA OS DADOS
			string contentData = ExtensibleMarkupSerial.serialize(interest, typeof(ArrayList));
			PlayerPrefs.SetString(INTEREST_KEY, contentData);
			PlayerPrefs.Save();
		}
	}
	
}
