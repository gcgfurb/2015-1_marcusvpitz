﻿using UnityEngine;
using System.Collections;

public interface Filter {
	Coordinate process(Coordinate coord);
}

public abstract class FilterNavigation : Filter {	

	public abstract Coordinate process(Coordinate coord);

	public static Coordinate operator *(FilterNavigation filter, Coordinate coord) {
		return filter.process(coord);
	}
	
	public static Coordinate operator *(Coordinate coord, FilterNavigation filter) {
		return filter.process(coord);
	}
}

/**
* Kalman Filter implementation
* @Author Marcus Vinicius Pitz
* @Date 28/03/2015
**/
public class KalmanFilter : FilterNavigation {

	private float q_meters = 1.0f;
	private float accuracy = 1.0f; //ACCURACY IN METERS
	private float timeStampOccur = 0.0f;
	private float variance = 0.0f;
	private Coordinate coord = null;

	public KalmanFilter(float accuracy, float velocity) {
		this.accuracy = accuracy;
		this.q_meters = velocity;
	}

	public override Coordinate process(Coordinate c) {		
		if (timeStampOccur == 0.0f) {
			//INITIALIZE
			timeStampOccur = getTime();
			//Estimativa inicial
			variance = this.accuracy * this.accuracy;	
			this.coord = c.clone();		
		} else {
			//PROCESS
			float timeInc_milliseconds = getTime() - this.timeStampOccur;
			if (timeInc_milliseconds > 0.0f) {
				//Variança inicial
				this.variance += timeInc_milliseconds * q_meters * q_meters;				
				this.timeStampOccur = getTime();
			}		
			//Ganho de Kalman
			float K = variance / (variance + accuracy * accuracy);
				//TODO Ganho de distanciamento: 
				//K²=(q_meters * timeInc_milliseconds)/ Haversine(this.coord, c)
				//K = K * K²
			//Nova estimativa
			this.coord = this.coord + ( K * ( c - this.coord ) );
			//Nova variança
			variance = (1 - K) * variance;			
		}
		return coord;
	}
	
	private float getTime() {
		return Time.realtimeSinceStartup;
	}
}
