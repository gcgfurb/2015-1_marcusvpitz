using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GyroscopeStats : MonoBehaviour {

	public Text texto;
	
	// Use this for initialization
	void Start () {
		Input.gyro.enabled = true;
	}
	
	// Update is called once per framesx
	void Update () {		
		//texto.text = "X=" + Input.gyro.userAcceleration.x + ", Y=" + Input.gyro.userAcceleration.y + ", Z=" + Input.gyro.userAcceleration.z;		
		this.transform.Translate(Input.gyro.userAcceleration.x, 0.0f, Input.gyro.userAcceleration.z);
	}
}
