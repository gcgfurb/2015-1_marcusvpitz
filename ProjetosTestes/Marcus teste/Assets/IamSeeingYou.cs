﻿using UnityEngine;
using System.Collections;

public class IamSeeingYou : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		Vector3 dif = PrefUtils.MAIN_CAMERA.position - this.transform.position;
		//this.transform.LookAt(PrefUtils.MAIN_CAMERA);
		this.transform.LookAt(this.transform.position - dif);
		//this.transform.rotation = Quaternion.Euler(this.transform.rotation.x, 0.0f, this.transform.rotation.z);
	}
}
