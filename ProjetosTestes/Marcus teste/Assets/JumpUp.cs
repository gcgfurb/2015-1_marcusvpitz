﻿using UnityEngine;
using System.Collections;

public class JumpUp : MonoBehaviour {

	private Vector3 max;
	private Vector3 min;
	
	private Vector3 who;
	
	private float beat;
	
	private readonly float TURN_BEAT = 2.0f;
	
	
	void Start() {
		max = this.transform.localPosition;
		min = Vector3.zero;
		who = min;
	}
	
	void Update () {
		if (beat <= TURN_BEAT) {
			this.transform.localPosition = Vector3.Slerp(this.transform.localPosition, who, TURN_BEAT * Time.deltaTime);
			beat += Time.deltaTime;
		} else {
			beat = 0.0f;
			who = (who == max ? min : max);
		}
	}
}
