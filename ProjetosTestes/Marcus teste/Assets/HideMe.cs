﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HideMe : MonoBehaviour {

	private string lastText;
	private Text myText;
	public float maxTime = 5.0f;
	private float time = 0.0f;

	// Use this for initialization
	void Start () {
		myText = this.GetComponent<Text>();
		lastText = myText.text;
	}
	
	// Update is called once per frame
	void Update () {
		//change
		if (lastText.CompareTo(myText.text) != 0) {
			time = 0.0f;
			myText.enabled = true;
			lastText = myText.text;			
		}
		
		if (time >= maxTime) {
			myText.enabled = false;
		} else {
			time += Time.deltaTime;
		}
	}
}
