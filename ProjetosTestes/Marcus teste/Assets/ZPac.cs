﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ZPac : MonoBehaviour {

	public Text texto;

	private int fg01 = -1;
	private int fg02 = -1;
	
	private Vector2 pos01 = Vector3.zero;
	private Vector2 pos02 = Vector3.zero;
	private SmoothFollowCSharp mainComponent;
	
	private float lastDistance = -1;

	void Start () {
		mainComponent = this.gameObject.GetComponent<SmoothFollowCSharp>();
	}
		
	void Update () {
		for(int i = 0; i < Input.touches.Length; i ++) {
			Touch touch=Input.touches[i];
			if(touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
			{
				if (fg01 != -1 && fg02 != -1) {
					if (fg01 == touch.fingerId || fg02 == touch.fingerId) {
						if (fg01 == touch.fingerId) {
							pos01 = touch.position;
						} else if (fg02 == touch.fingerId) {
							pos02 = touch.position;
						}
						float dist = Vector2.Distance(pos01, pos02);
						if (dist > this.lastDistance) {
							less ();
						} else if (dist < this.lastDistance) {
							more ();
						}
						this.lastDistance = dist;
					}
				}
			} else if(touch.phase == TouchPhase.Began)
			{
				if (fg01 == -1) {
					fg01 = touch.fingerId;
					pos01 = touch.position;
				} else if (fg02 == -1) {
					fg02 = touch.fingerId;
					pos02 = touch.position;
				}
			} else if(touch.phase == TouchPhase.Ended)
			{	
				if (fg01 == touch.fingerId) {
					fg01 = -1;
					pos01 = Vector3.zero;
				} else if (fg02 == touch.fingerId) {
					fg02 = -1;
					pos02 = Vector3.zero;
				}
			}
		}			
	}
	
	readonly float vD = 0.08f;
	readonly float vH = 0.05f;
	void more() {
		mainComponent.distance += vD;
		mainComponent.height += vH;
	}
	
	void less() {
		mainComponent.distance -= vD;
		if (mainComponent.distance < 0.0f) {
			mainComponent.distance = 0.0f;
		}
		mainComponent.height -= vH;
		if (mainComponent.height < 2.5f) {
			mainComponent.height = 2.5f;
		}		
	}
}
