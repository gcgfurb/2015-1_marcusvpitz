﻿using UnityEngine;
using System.Collections;

public class CenterMap : MonoBehaviour {

	public float latitude = -26.894809f;
	public float longitude = -49.094857f;
	
	/**
	* TODO Gambiarra para acessar os dados do CenterMap
	**/
	public static Coordinate mapCenter;
	public static Transform centerCoord;
	


	public void Awake() {
		mapCenter = new Coordinate(this.latitude, this.longitude);
		centerCoord = this.transform;
	}
	
	// Use this for initialization
	void Start () {		
	}
	
	// Update is called once per frame
	void Update () {	
	}
}
